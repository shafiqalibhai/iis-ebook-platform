<?php // Magento� commercial extension �� by Tomasz So�tysiak <contact@third-dimension.eu>
require_once 'Mage/Downloadable/controllers/DownloadController.php';
class ThirdDimension_Ebook_DownloadController extends Mage_Downloadable_DownloadController {
	public function linkAction() {
		$item = Mage::getModel('downloadable/link_purchased_item')->load($this->getRequest()->getParam('id',0), 'link_hash');
		$link = $item->getLinkFile();
		
		if (($item->getLinkType()==='file') && (strtoupper(substr($link,-3))==='PDF')) {
			$user = Mage::getSingleton('customer/session')->getCustomer();
			if ((strlen($user->getName())>0) && (strlen($user->getEmail())>0))
			if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$",$user->getEmail())) die('Invalid user email adress!');
			else {
				require_once('libs/tfpdf.php');
				require_once('libs/fpdi_protection.php');
				
				$links = 'media/downloadable/files/links';
				if (!file_exists($links.$link)) die('Your linked PDF file does not exists in '.$links.$link.'. Please reupload file in admin panel.');
				if (!file_exists($links.$link.'~')) if (!copy($links.$link,$links.$link.'~')) die('Cannot backup orginal PDF file '.$links.$link.' to '.$links.$link.'~');
				
				$pdf = new FPDI_Protection();
				$pages = $pdf->setSourceFile($links.$link.'~');				
				for ($p=1;$p<=$pages;$p++) {
					$ipdf = $pdf->ImportPage($p);
					$ipdfsize = $pdf->getTemplateSize($ipdf);
					$pdf->AddPage('P',array($ipdfsize['w'],$ipdfsize['h']));
					$pdf->useTemplate($ipdf,0,0,0,0,true);
					$pdf->SetXY(10,10); // Set position to 10 mm from the left and 10 mm from the top of each page in PDF
					$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true); // Sets font to Unicode DejaVu
					$pdf->SetFont('DejaVu','',0.5); // Sets font size to 10 pt, info: www.fpdf.org/en/script/script92.php
					$pdf->SetTextColor(0,0,0); // Set font color to black - http://cloford.com/resources/colours/500col.htm
					$purchase = Mage::getModel('downloadable/link_purchased')->load($item->getPurchasedId(), 'purchased_id');
					$pdf->Write(0,'Purchased by: '.$user->getName().'. Order ID: #'.($purchase->getOrderIncrementId()).'.');
				}
				$pdf->SetProtection(array(),$user->getEmail()); // Limit all rights except reading, f.e. to allow printing use array('print')
				$pdf->Output($links.$link);
			}
		}
		parent::linkAction();
	}
}